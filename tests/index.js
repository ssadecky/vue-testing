import Vue from 'vue';

Vue.config.productionTip = false;

// require all test files (files that ends with .spec.js)
const testsContext = require.context('./specs', true, /\.spec$/);
testsContext.keys().forEach(testsContext);

// this is where *I think* code for coverage is required
// all expected files are required, but coverage isn't reported as expected
const srcContext = require.context('../src', true, /(\.js|\.vue)$/);
srcContext.keys().forEach(srcContext);
const webpackConf = require('@vue/cli-service/webpack.config');
const tests = 'index.js';

const karmaConfig = {
        browsers: ['ChromeHeadless'],
        frameworks: ['mocha', 'sinon-chai'],
        files: [
            tests,
        ],

        exclude: [],

        webpack: webpackConf,
        webpackMiddleware: {
            noInfo: true,
            stats: 'errors-only'
        },
        preprocessors: {
            tests: ['webpack', 'sourcemap'],
        },

        client: {
            captureConsole: true
        },

        reporters: ['spec', 'coverage'],
        specReporter: {
            showSpecTiming: true
        },
        reportSlowerThan: 25,

        coverageReporter: {
            dir: './coverage',
            reporters: [
                { type: 'lcov', subdir: '.' },
                { type: 'text-summary' }
            ]
        },

        port: 9876,
        colors: true,
        autoWatch: true,
};

karmaConfig.preprocessors[tests] = ['webpack', 'sourcemap'];

module.exports = function (config) {
    config.set(karmaConfig);
}
import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import SignUpForm from '../../src/components/SignUpForm.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('SignUpForm component', () => {
    describe('Methods', () => {
        let actions;
        let state;
        let store;
        let computed;

        beforeEach(() => {
            actions = {
                doSignUp: sinon.spy(),
            };

            state = {
                state: {
                    myUser: {
                        email: '',
                        password: '',
                    },
                }
            };

            store = new Vuex.Store({
                modules: {
                    user: {
                        namespaced: true,
                        state,
                        actions
                    },
                },
            });

            computed = {
                myUser: () => {
                    return {
                        email: '',
                    };
                }
            }
        });

        afterEach(() => {
            // Restore the default sandbox here
            sinon.restore();
        });

        it('Method "doSignUp" was called', () => {
            const wrapper = mount(SignUpForm, {
                store,
                computed,
                localVue
            });
            const callback = actions.doSignUp;

            wrapper.find('form').trigger('submit');

            expect(callback.calledOnce).to.equal(true);
        });
    });
});

import { mount } from '@vue/test-utils';
import HelloWorld from '../../src/components/HelloWorld.vue';

describe('HelloWorld', () => {
    describe('Props', () => {
        it('Prop: Message should be "Hello"', () => {
            const wrapper = mount(HelloWorld)

            assert.equal(wrapper.vm.msg, 'Hello');
        });
    })
});

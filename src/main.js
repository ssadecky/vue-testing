// Import styles
import './assets/less/app.less'

// Import Js
import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router/router'
import store from './store/index'

// Components
import Login from './views/Login'
import LoginForm from './components/LoginForm'
import SignUp from './views/SignUp'
import SignUpForm from './components/SignUpForm'
import Hello from './components/HelloWorld'

import { firebaseConfig } from './const/firebase'
import firebase from 'firebase'

Vue.config.productionTip = false

Vue.component('login-form', LoginForm)
Vue.component('login', Login)
Vue.component('signup-form', SignUpForm)
Vue.component('signup', SignUp)
Vue.component('hello', Hello)

firebase.initializeApp(firebaseConfig);

router.beforeEach((to, from, next) => {
    const isLogged = store.getters['user/isLogged']
    const auth = to.matched.some(record => record.meta.auth)

    console.log(isLogged)

    if (auth && !isLogged) {
        next('login')
    }
    else {
        next()
    }
})

new Vue({
    router,
    store,

    render: h => h(App),
}).$mount('#app')

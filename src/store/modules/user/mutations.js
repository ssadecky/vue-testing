import { storage} from '../../../utils/storage'

/**
 * Send some data that nobody knows what they actualy are for.
 * @param state
 * @param data
 */
export const setUser = (state, data) => {
    console.log('Mutation setUser')
    state.myUser = data
    storage.setItem('user', JSON.stringify(data))
}

export const setTokens = (state, data) => {
    console.log('Mutation TOKENS:', data)
    state.tokens = data
    storage.setItem('tokens', JSON.stringify(data))
}
import { storage } from '../../../utils/storage'

import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'

const state = {
    myUser: JSON.parse(storage.getItem('user')) || {},
    tokens: JSON.parse(storage.getItem('tokens')) || {},
}

export default {
    state,
    getters,
    actions,
    mutations,
}

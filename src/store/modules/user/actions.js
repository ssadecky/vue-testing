import firebase from 'firebase'
import router from '../../../router/router'

export const doSignUp = ({ commit, dispatch }, form) => {
    firebase.auth()
        .createUserWithEmailAndPassword(form.email, form.password)
        .then((data) => {
            console.log('User registerred')
            commit('setUser', data.user)
            dispatch('doTokens', data.user)

            router.push('/hello')
        })
        .catch((error) => {
            const errorCode = error.code
            const errorMessage = error.message
            if (errorCode === 'auth/weak-password') {
                alert('The password is too weak.')
            } else {
                alert(errorMessage)
            }
            console.log('Toto je error:', error)
        });
}

export const doLogout = ({ commit }) => {
    firebase.auth().signOut()
    commit('setUser', {})
    commit('setTokens', {})
}

export const doLogIn = ({ commit, dispatch }, form) => {
    // firebase.auth()
    //     .signInWithEmailAndPassword(form.email, form.password)
    //     .then((data) => {
    //         console.log('User logged')
    //         commit('setUser', data.user)
    //         dispatch('doTokens', data.user)
    //
    //         router.push('/hello')
    //     })
    //     .catch((error) => {
    //         const errorCode = error.code
    //         const errorMessage = error.message
    //         if (errorCode === 'auth/weak-password') {
    //             alert('The password is too weak.')
    //         } else {
    //             alert(errorMessage)
    //         }
    //         console.log('Toto je error:', error)
    //     });

    const user = {
        email: 'jozefusmail@mail.com',
        token: 'Token11111'
    }

    commit('setUser', user)
    commit('setTokens', user)
}

export const doTokens = ({ commit }, data) => {
    commit('setTokens', Object.keys(data)
        .filter(key => key.includes('Token'))
        .reduce((obj, key) => {
            obj[key] = data[key]
            return obj
        }, {})
    )
}

const obj = {
    name: 'marek',
}
const obj2 = {
    age: '32'
}
const obj3 = {}

Object.assign(obj3, obj, obj2)


import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'

import user from './modules/user'

Vue.use(Vuex)

const state = {
    options: {},
}

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions,

    modules: {
        user: {
            namespaced: true,
            ...user,
        },
    },

})

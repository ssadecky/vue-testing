import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import SignUp from '../views/SignUp.vue'
import About from '../views/About.vue'

import Hello from '../components/HelloWorld.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '*',
            redirect: '/login',
        },
        {
            path: '/',
            name: 'home',
            component: Home,

            children: [{
                path: '/hello',
                component: Hello,
                meta: {
                    auth: true,
                },
            }]
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/sign-up',
            name: 'sign-up',
            component: SignUp,

            beforeEnter(to, from, next) {
                if (store.getters['user/isLogged']) {
                    return next('/')
                }

                next()
            }
        },
        {
            path: '/about',
            name: 'about',
            component: About,
            meta: {
                auth: true,
            }
        },
    ]
})
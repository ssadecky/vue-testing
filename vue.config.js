module.exports = {
  assetsDir: 'public',
  runtimeCompiler: true,

  css: {
    extract: false
  },

  lintOnSave: undefined
}